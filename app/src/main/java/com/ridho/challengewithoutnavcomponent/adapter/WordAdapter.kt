package com.ridho.challengewithoutnavcomponent.adapter

import android.content.Context
import android.content.Intent
import android.net.Uri
import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.ridho.challengewithoutnavcomponent.R
import com.ridho.challengewithoutnavcomponent.databinding.ItemListBinding.inflate
import com.ridho.challengewithoutnavcomponent.databinding.ItemListBinding
import com.ridho.challengewithoutnavcomponent.utils.Constants.GOOGLE_SEARCH

class WordAdapter(private val letter: String, context: Context) :
    RecyclerView.Adapter<WordAdapter.WordViewHolder>() {

    private val filter: List<String>

    inner class WordViewHolder(private val binding: ItemListBinding) : RecyclerView.ViewHolder(binding.root) {
        fun bind(word: String) {
            binding.button.text = word

            binding.button.setOnClickListener {
                val uri = Uri.parse("${GOOGLE_SEARCH}${word}")
                val intent = Intent(Intent.ACTION_VIEW, uri)
                itemView.context.startActivity(intent)
            }
        }
    }
    // ngisi untuk layout yang digunakan
    // membuat view holder berisi layout item (item.list)
    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): WordViewHolder =
        WordViewHolder(inflate(LayoutInflater.from(parent.context), parent, false))

    override fun onBindViewHolder(holder: WordViewHolder, position: Int) = holder.bind(filter[position])

    override fun getItemCount(): Int = filter.size

    init {
        // untuk mendapatkan string resource dari array data_word
        val words = context.resources.getStringArray(R.array.data_word).toList()
        // untuk memfilter dimulai dari huruf awal
        filter = words.filter { it.startsWith(letter, ignoreCase = true) }
    }

}