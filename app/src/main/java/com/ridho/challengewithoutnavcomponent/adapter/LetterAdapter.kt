package com.ridho.challengewithoutnavcomponent.adapter

import android.os.Bundle
import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.appcompat.app.AppCompatActivity
import androidx.recyclerview.widget.RecyclerView
import com.ridho.challengewithoutnavcomponent.fragment.letter.detail.DetailLetterFragment
import com.ridho.challengewithoutnavcomponent.R
import com.ridho.challengewithoutnavcomponent.databinding.ItemListBinding.inflate
import com.ridho.challengewithoutnavcomponent.databinding.ItemListBinding
import com.ridho.challengewithoutnavcomponent.utils.Constants.EXTRA_WORD

class LetterAdapter: RecyclerView.Adapter<LetterAdapter.LetterViewHolder>() {

    private val list = ('A').rangeTo('Z').toList()

    inner class LetterViewHolder(private val binding: ItemListBinding) :
        RecyclerView.ViewHolder(binding.root) {
        fun bind(list: Char) {
            binding.button.text = list.toString()

            binding.button.setOnClickListener {
                val appCompatActivity = it.context as AppCompatActivity
                val mFragmentManager = appCompatActivity.supportFragmentManager
                val mDetailLetterFragment = DetailLetterFragment()
                val fragment =
                    mFragmentManager.findFragmentByTag(DetailLetterFragment::class.java.simpleName)

                if (fragment !is DetailLetterFragment) {
                    val bundle = Bundle()
                    bundle.putString(EXTRA_WORD, list.toString())
                    mDetailLetterFragment.arguments = bundle

                    mFragmentManager
                        .beginTransaction()
                        .replace(
                            R.id.frameContainer,
                            mDetailLetterFragment,
                            DetailLetterFragment::class.java.simpleName
                        )
                        .addToBackStack(null)
                        .commit()
                }
            }
        }
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): LetterViewHolder =
        LetterViewHolder(inflate(LayoutInflater.from(parent.context), parent, false))

    override fun onBindViewHolder(holder: LetterViewHolder, position: Int) =
        holder.bind(list[position])

    override fun getItemCount(): Int = list.size
}