package com.ridho.challengewithoutnavcomponent.utils

object Constants {
    const val EXTRA_WORD = "EXTRA_WORD"
    const val GOOGLE_SEARCH = "https://www.google.com/search?q="
}