package com.ridho.challengewithoutnavcomponent

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import com.ridho.challengewithoutnavcomponent.fragment.letter.ListLetterFragment

class MainActivity : AppCompatActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        val mFragmentManager = supportFragmentManager
        val mHomeFragment = ListLetterFragment()
        val fragment = mFragmentManager.findFragmentByTag(ListLetterFragment::class.java.simpleName)

        if (fragment !is ListLetterFragment) {
            mFragmentManager
                .beginTransaction()
                .add(R.id.frameContainer, mHomeFragment, ListLetterFragment::class.java.simpleName)
                .commit()
        }
    }
}