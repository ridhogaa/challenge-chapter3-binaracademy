package com.ridho.challengewithoutnavcomponent.fragment.letter.detail

import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toolbar
import androidx.appcompat.app.AppCompatActivity
import androidx.recyclerview.widget.LinearLayoutManager
import com.ridho.challengewithoutnavcomponent.R
import com.ridho.challengewithoutnavcomponent.adapter.WordAdapter
import com.ridho.challengewithoutnavcomponent.databinding.FragmentDetailLetterBinding
import com.ridho.challengewithoutnavcomponent.utils.Constants.EXTRA_WORD

class DetailLetterFragment : Fragment() {
    private var _binding: FragmentDetailLetterBinding? = null
    private val binding get() = _binding!!
    private lateinit var letter: String

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        _binding = FragmentDetailLetterBinding.inflate(inflater, container, false)
        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        binding.rvWord.layoutManager = LinearLayoutManager(
            requireContext()
        )

        arguments?.let { letter = it.getString(EXTRA_WORD).toString() }

        binding.rvWord.adapter = WordAdapter(letter, requireContext())
        val title = activity as AppCompatActivity
        title.supportActionBar?.title = "Words That Start With $letter"
    }

    override fun onDestroyView() {
        super.onDestroyView()
        _binding = null
    }
}