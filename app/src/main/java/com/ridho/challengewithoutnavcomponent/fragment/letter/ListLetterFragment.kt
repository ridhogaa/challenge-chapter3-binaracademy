package com.ridho.challengewithoutnavcomponent.fragment.letter

import android.os.Bundle
import android.view.*
import androidx.appcompat.app.AppCompatActivity
import androidx.fragment.app.Fragment
import androidx.core.content.ContextCompat
import androidx.core.view.MenuHost
import androidx.core.view.MenuProvider
import androidx.lifecycle.Lifecycle
import androidx.recyclerview.widget.GridLayoutManager
import androidx.recyclerview.widget.LinearLayoutManager
import com.ridho.challengewithoutnavcomponent.R
import com.ridho.challengewithoutnavcomponent.adapter.LetterAdapter
import com.ridho.challengewithoutnavcomponent.databinding.FragmentListLetterBinding

class ListLetterFragment : Fragment() {
    private var _binding: FragmentListLetterBinding? = null
    private val binding get() = _binding!!
    private var isListMode = true

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        _binding = FragmentListLetterBinding.inflate(inflater, container, false)
        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        binding.rvLetter.layoutManager = LinearLayoutManager(
            requireContext()
        )

        binding.rvLetter.adapter = LetterAdapter()

        switchMode()

        val menuHost: MenuHost = requireActivity()
        menuHost.addMenuProvider(object : MenuProvider {
            override fun onCreateMenu(menu: Menu, menuInflater: MenuInflater) {
                menuInflater.inflate(R.menu.item_menu, menu)
                changeIcon(menu.findItem(R.id.switchMode))
            }

            override fun onMenuItemSelected(menuItem: MenuItem): Boolean {
                return when (menuItem.itemId) {
                    R.id.switchMode -> {
                        isListMode = !isListMode
                        switchMode()
                        changeIcon(menuItem)

                        return true
                    }
                    else -> false
                }
            }
        }, viewLifecycleOwner, Lifecycle.State.RESUMED)
        val title = activity as AppCompatActivity
        title.supportActionBar?.title = "Words"
    }

    override fun onDestroyView() {
        super.onDestroyView()
        _binding = null
    }

    private fun changeIcon(menuItem: MenuItem?) {
        menuItem?.icon =
            if (isListMode)
                ContextCompat.getDrawable(this.requireContext(), R.drawable.ic_grid_mode)
            else ContextCompat.getDrawable(this.requireContext(), R.drawable.ic_list_mode)
    }

    private fun switchMode() {
        binding.rvLetter.layoutManager =
            if (isListMode)
                LinearLayoutManager(requireContext())
            else
                GridLayoutManager(requireContext(), 2)
    }
}